//
//  ViewController.swift
//  projectGoogleMaps
//
//  Created by User on 12/15/18.
//  Copyright © 2018 digiCollect. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces


class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate{
    
    
    //MARK:- Outlet Properties
    @IBOutlet weak var myView: GMSMapView!
    
    //MARK:- Class Properties
    let locationManager = CLLocationManager()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var activityIndicator = UIActivityIndicatorView()
    let place: GMSPlace! = nil
    
    var mapView: GMSMapView!
    
    var destCord = CLLocationCoordinate2D()
    var marker = GMSMarker()
    var destinatiomnMarker = GMSMarker()
    
    //MARK:- Constant variables
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var index = 0
    var count = 0
    
    var timerAnimation: Timer!
    var animated = true
    
    var path = GMSMutablePath()
    var oldCoordinate = CLLocationCoordinate2D()
    var newCoordinate = CLLocationCoordinate2D()
    var destinationArray = [CLLocationCoordinate2D]()
    var startNavigation = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 350.0, height: 45.0))
        
        // subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // This makes the view area include the nav bar even though it is opaque.
        // Adjust the view placement down.
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        
        startNavigation = UIButton(frame: CGRect(x: self.view.frame.size.width/3, y: self.view.frame.size.height-50, width: 150, height: 50))
        startNavigation.backgroundColor = UIColor.blue
        startNavigation.setTitle("Start Navigation", for: .normal)
        startNavigation.setTitleColor(UIColor.white, for: .normal)
        startNavigation.addTarget(self, action: #selector(startAnimation), for: .touchUpInside)
        self.view.addSubview(startNavigation)
        startNavigation.isHidden = true
        
    }
    
    
    @objc func startAnimation(button: UIButton){
        startNavigation.isHidden = true
        print("Navigationstarted")
        
        moveMarker()
        //updateMarker()
        
        print("NavigationEnded")
        
    }
    
    /**This method is to update location on map.
     - Returns: Nothing
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.showCurrentLocationOnMap()
        self.locationManager.stopUpdatingLocation()
        self.locationManager.startUpdatingHeading()
    }
    
    
    
    /**This method is to show user current location on map.
     - Returns: Nothing
     */
    func showCurrentLocationOnMap() {
        
        let camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 14)
        
        mapView = GMSMapView.map(withFrame: self.myView.frame, camera: camera)
        
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        //let marker1 = GMSMarker()
        marker.position = camera.target
        marker.snippet = "current location"
        //marker.icon = self.imageWithImage(image: UIImage(named: "car")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.map = mapView
        
        self.myView.addSubview(mapView)
    }
    
    
    /**This method is to scale the marker image to show on map.
     - Returns: UIImage
     */
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /**This method is to add search places button action.
     - Parameter Sender: Any.
     - Returns: Nothing
     */
    @IBAction func searchPlaces(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
    }
    
}


//extensions for viewcontroller
extension ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("place latitude: \(place.coordinate.latitude)")
        print("place longitude: \(place.coordinate.longitude)")
        
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        
        //add marker to the selected position
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14)
        
        mapView = GMSMapView.map(withFrame: self.myView.frame, camera: camera)
        
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        //self.destCord = CLLocationCoordinate2DMake(latitude, longitude)
        //destinatiomnMarker.iconView = markerView
        destinatiomnMarker.position = camera.target
        destinatiomnMarker.snippet = "selected location"
        destinatiomnMarker.map = mapView
        
        self.myView.addSubview(mapView)
        
        dismiss(animated: true, completion: nil)
        
        drawRoute()
        playAnimation()
    }
    
    
    
    /**This method is to draw routes from current location to selected location.
     - Returns: Nothing
     */
    func drawRoute() {
        
        let origin = "\(self.locationManager.location?.coordinate.latitude ?? 0),\(self.locationManager.location?.coordinate.longitude ?? 0)"
        let destination = "\(latitude),\(longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBfals35uQG_xRa7r3F8iaZQUloGefBihs"
        print(url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        guard let requestUrl = URL(string   :url) else { return }
        let request = URLRequest(url:requestUrl)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            DispatchQueue.main.async {
                if error != nil {
                    print(error!.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    
                }else{
                    do {
                        
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                            
                            if let routes = json["routes"] as? [Any]  {
                                //print(routes)
                                if let route = routes[0] as? [String: Any] {
                                    //print(route)
                                    if let legs = route["legs"] as? [Any]{
                                        // print(legs)
                                        if let leg = legs[0] as? [String: Any]{
                                            // print(leg)
                                            if let steps = leg["steps"] as? [Any]{
                                                //print("steps:\(steps)")
                                                
                                                for step in steps {
                                                    print(step)
                                                    let routePolyline:NSDictionary = (step as! NSDictionary).value(forKey: "polyline") as! NSDictionary
                                                    //print("routePolyline is : \(routePolyline)")
                                                    let points = routePolyline.object(forKey: "points") as! String
                                                    //print("points are: \(points)")
                                                    
                                                    DispatchQueue.main.async  {
                                                        
                                                        self.showPath(polyStr: points)
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }catch{
                        print("error in JSONSerialization")
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
            }
        })
        task.resume()
   

    }
    
    
    /**This method is to show paths between route points
     - Parameter Sender: String.
     - Returns: Nothing
     */
    func showPath(polyStr :String){
        //let mapView = GMSMapView()
        let path = GMSMutablePath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = .red
        polyline.map = mapView
        
        if(animated){
            //self.animatePolylinePath(path: path!)
        }
    }
    
    
    /**This method is to animate polyline route points
     - Parameter path: GMSMutablePath.
     - Returns: Nothing
     */
    func animatePolylinePath(path: GMSMutablePath) {
        
        var pos: UInt = 0
        var animationPath = GMSMutablePath()
        let animationPolyline = GMSPolyline()
        self.timerAnimation = Timer.scheduledTimer(withTimeInterval: 0.003, repeats: true) { timer in
            
            pos += 1
            if(pos >= path.count()){
                pos = 0
                animationPath = GMSMutablePath()
                animationPolyline.map = nil
            }
            animationPath.add(path.coordinate(at: pos))
            animationPolyline.path = animationPath
            animationPolyline.strokeColor = UIColor.green
            animationPolyline.strokeWidth = 3
            animationPolyline.map = self.mapView
        }
    }
    
    
    /**This method is to stop animation of polylines.
     - Parameter path: GMSMutablePath.
     - Returns: Nothing
     */
    func stopAnimatePolylinePath() {
        
        self.timerAnimation.invalidate()
    }
    
    
    /**This method is to animate marker along polyline points
     - Parameter:- Nothing
     - Returns: Nothing
     */
    func playAnimation() {
        
        let origin = "\(self.locationManager.location?.coordinate.latitude ?? 0),\(self.locationManager.location?.coordinate.longitude ?? 0)"
        let destination = "\(latitude),\(longitude)"
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBfals35uQG_xRa7r3F8iaZQUloGefBihs"
        print(url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        guard let requestUrl = URL(string   :url) else { return }
        let request = URLRequest(url:requestUrl)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            DispatchQueue.main.async {
                if error != nil {
                    print(error!.localizedDescription)
                    
                }else{
                    do {
                        
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                            
                            if let routes = json["routes"] as? [Any]  {
                                //print(routes)
                                if let route = routes[0] as? [String: Any] {
                                    //print(route)
                                    if let legs = route["legs"] as? [Any]{
                                        // print(legs)
                                        if let leg = legs[0] as? [String: Any]{
                                            // print(leg)
                                            if let steps = leg["steps"] as? [Any]{
                                                // print("steps:\(steps)")
                                                
                                                for step in steps {
                                                    //print(step)
                                                    let endLocation:NSDictionary = (step as! NSDictionary).value(forKey: "end_location") as! NSDictionary
                                                    print("endLocation is : \(endLocation)")
                                                    
                                                    let endLocationLatitude = endLocation.object(forKey: "lat") as! CLLocationDegrees
                                                    print(endLocationLatitude)
                                                    
                                                    let endLocationLongitude = endLocation.object(forKey: "lng") as! CLLocationDegrees
                                                    print(endLocationLongitude)
                                                    
                                                    let startLocation:NSDictionary = (step as! NSDictionary).value(forKey: "start_location") as! NSDictionary
                                                    print("startLocation is : \(startLocation)")
                                                    
                                                    let startLocationLatitude = startLocation.object(forKey: "lat") as! CLLocationDegrees
                                                    print(startLocationLatitude)
                                                    
                                                    let startLocationLongitude = startLocation.object(forKey: "lng") as! CLLocationDegrees
                                                    print(startLocationLongitude)
                                                    
//                                                    let destinationLocation = CLLocationCoordinate2DMake(12.971124, 77.5976807)
                                                    
                                                   let destinationLocation = CLLocationCoordinate2DMake(endLocationLatitude,endLocationLongitude)
                                                    
                                                    print("destinationLocation:\(destinationLocation)")
                                                    
                                                    self.destinationArray.append(destinationLocation)
                                                    
                                                    print("destinationArray:\(self.destinationArray)")

                                                    let startLocationDestination = CLLocationCoordinate2DMake(startLocationLatitude, startLocationLongitude)
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }catch{
                        print("error in JSONSerialization")
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
            }
        })
        task.resume()
        startNavigation.isHidden = false
    }
    
    
    func moveMarker()
    {
        count = destinationArray.count
        print(count)
        
        //print(destinationArray[index])
        let nextPoint = destinationArray[index]
        print(nextPoint)
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(5.0)
        self.marker.position = nextPoint
        self.marker.map = self.mapView
        self.marker.appearAnimation = .pop
        CATransaction.commit()

        
//        for state in destinationArray {
////            let state_marker = GMSMarker()
////            state_marker.position = CLLocationCoordinate2D(latitude: state.latitude, longitude: state.longitude)
////            state_marker.title = "new point"
////            state_marker.snippet = "Hey, this is new point"
////            state_marker.map = mapView
//
//            CATransaction.begin()
//            CATransaction.setAnimationDuration(5.0)
//            self.marker.position = CLLocationCoordinate2D(latitude: state.latitude, longitude: state.longitude)
//            self.marker.map = self.mapView
//            self.marker.appearAnimation = .pop
//            CATransaction.commit()
//
//        }
      
        
        //intially index is 0
//        index += 1
        
//        if index <= count-1 {
//
//            //delay(5, closure: {})
//
//            self.moveMarker()
//        }
        
    }
    
    /**This method is to show animation of marker
     - Returns: Nothing
     */
    func updateMarker() {
        
        let count: Double = Double(self.destinationArray.count)
        CATransaction.begin()
        CATransaction.setAnimationDuration(20.0/count)
         marker.position = destinationArray[index]
        CATransaction.setCompletionBlock
            {
                if self.index == 0{
                    self.destinationArray.removeAll()
                    return;
                }
                else{
                    self.updateMarker()
                }

        }

        let transition = CATransition()
        transition.type = CATransitionType.moveIn
        //self.originMarker.layer.addAnimation(transition, forKey: kCATransition)
       
        CATransaction.commit()

        index = index < destinationArray.count - 1 ? index + 1 : 0
    }
    
    //MARK: Delay func
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


// Handle the user's selection.
extension ViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        
        //add marker to the selected position
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14)
        
        mapView = GMSMapView.map(withFrame: self.myView.frame, camera: camera)
        
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "selected location"
        marker.map = mapView
        
        self.myView.addSubview(mapView)
        
        dismiss(animated: true, completion: nil)
        
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

